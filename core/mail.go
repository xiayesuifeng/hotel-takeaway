package core

import (
	"net/smtp"
)

func SendVerificationCodeMail(toEmail,verificationCode string) error {
	username := Conf.Smtp.Username
	auth := smtp.PlainAuth("", username, Conf.Smtp.Password, Conf.Smtp.Host)
	subject := "网上酒店外卖"
	body := "您的验证码是: "+verificationCode

	msg := []byte("To: " + toEmail + "\nFrom: " + username + "\nSubject: " + subject + "\n\n" + body)
	return smtp.SendMail(Conf.Smtp.Host+":25", auth, Conf.Smtp.Username, []string{toEmail}, msg)
}
