package main

import (
	"encoding/json"
	"flag"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"gitlab.com/xiayesuifeng/hotel-takeaway/address"
	"gitlab.com/xiayesuifeng/hotel-takeaway/category"
	"gitlab.com/xiayesuifeng/hotel-takeaway/controller"
	"gitlab.com/xiayesuifeng/hotel-takeaway/core"
	"gitlab.com/xiayesuifeng/hotel-takeaway/database"
	"gitlab.com/xiayesuifeng/hotel-takeaway/food"
	"gitlab.com/xiayesuifeng/hotel-takeaway/order"
	"gitlab.com/xiayesuifeng/hotel-takeaway/userCenter"
	"log"
	"os"
	"strconv"
	"strings"
)

var (
	port = flag.Int("p", 8080, "port")
	help = flag.Bool("h", false, "help")
)

func main() {
	router := gin.Default()

	store := cookie.NewStore([]byte("hotel-takeaway"))
	router.Use(sessions.Sessions("hotel-takeaway-session", store))

	apiRouter := router.Group("/api", checkPermissionMiddleware)

	{
		appApiC := &controller.AppApi{}

		apiRouter.POST("/login", appApiC.Login)
		apiRouter.POST("/logout", appApiC.Logout)
		apiRouter.POST("/signup", appApiC.Signup)
	}

	{
		addressRouter := apiRouter.Group("/address")

		addressC := &controller.Address{}

		addressRouter.GET("", addressC.Gets)
		addressRouter.GET("/:id", addressC.Get)
		addressRouter.POST("", addressC.Post)
		//addressRouter.PUT("/:id")
		addressRouter.DELETE("/:id", addressC.Delete)
	}

	{
		categoryRouter := apiRouter.Group("/category")

		categoryC := &controller.Category{}

		categoryRouter.GET("", categoryC.Gets)
		categoryRouter.GET("/:id", categoryC.Get)
		categoryRouter.POST("", categoryC.Post)
		categoryRouter.PUT("/:id", categoryC.Put)
		categoryRouter.DELETE("/:id", categoryC.Delete)
	}

	{
		orderRouter := apiRouter.Group("/order")

		orderC := &controller.Order{}

		orderRouter.GET("", orderC.Gets)
		orderRouter.GET("/:id", orderC.Get)
		orderRouter.POST("", orderC.Post)
		orderRouter.PATCH("/pay/:id",orderC.Pay)
		orderRouter.PATCH("/complete/:id",orderC.Complete)
		orderRouter.PATCH("/cancel/:id",orderC.Cancel)
		orderRouter.DELETE("/:id",orderC.Delete)
	}

	{
		foodRouter := apiRouter.Group("/food")

		foodC := &controller.Food{}

		foodRouter.GET("", foodC.Gets)
		foodRouter.GET("/id/:id", foodC.Get)
		foodRouter.GET("/category/:category_id", foodC.GetByCategory)
		foodRouter.POST("", foodC.Post)
		foodRouter.PUT("/:id", foodC.Put)
		foodRouter.DELETE("/:id", foodC.Delete)
	}

	{
		uCenterRouter := apiRouter.Group("/userCenter")

		uCenterC := &controller.UserCenter{}

		uCenterRouter.GET("/user", uCenterC.Get)
		uCenterRouter.POST("/user", uCenterC.Post)
		uCenterRouter.DELETE("/user/:id", uCenterC.Delete)
		uCenterRouter.PATCH("/user/:id", uCenterC.Put)

		uCenterRouter.POST("/group", uCenterC.AddGroup)
		uCenterRouter.DELETE("/group/:name", uCenterC.DeleteGroup)
		uCenterRouter.GET("/group", uCenterC.GetGroup)
	}

	router.Use(static.Serve("/", static.LocalFile("web", false)))
	router.NoRoute(func(c *gin.Context) {
		if !strings.Contains(c.Request.RequestURI,"/api") {
			path := strings.Split(c.Request.URL.Path, "/")
			if len(path) > 1 {
				c.File("web/index.html")
				return
			}
		}
	})

	router.Run(":" + strconv.Itoa(*port))
}

func checkPermissionMiddleware(ctx *gin.Context) {
	if strings.Contains(ctx.Request.RequestURI, "/api/food") &&
		ctx.Request.Method != "GET" {
		checkPermission(ctx, "admin")
	} else if strings.Contains(ctx.Request.RequestURI, "/api/order") {
		checkPermission(ctx, "users")
	} else if strings.Contains(ctx.Request.RequestURI, "/api/address") {
		checkPermission(ctx, "users")
	} else if strings.Contains(ctx.Request.RequestURI, "/api/category") &&
		ctx.Request.Method != "GET" {
		checkPermission(ctx, "admin")
	} else if strings.Contains(ctx.Request.RequestURI, "/api/userCenter") {
		if ctx.Request.Method == "PATCH" &&
			strings.Contains(ctx.Request.RequestURI, "/api/userCenter/user") {
			checkPermission(ctx, "insider")
		} else {
			checkPermission(ctx, "admin")
		}
	} else {
		ctx.Next()
	}
}

func checkPermission(ctx *gin.Context, group string) {
	session := sessions.Default(ctx)

	tmp := session.Get("username")
	if tmp == nil {
		ctx.JSON(200, gin.H{
			"code":    101,
			"message": "unauthorized",
		})

		ctx.Abort()
	}

	username := tmp.(string)

	user, err := userCenter.GetUserByName(username)
	if err != nil {
		ctx.JSON(200, gin.H{
			"code":    101,
			"message": "user no found",
		})

		ctx.Abort()
	}

	if group != "admin" {
		if user.HasGroup("admin") {
			ctx.Next()
			return
		}
	}

	if !user.HasGroup(group) {
		ctx.JSON(200, gin.H{
			"code":    101,
			"message": "permission denied",
		})
		ctx.Abort()
	}

	ctx.Next()

}

func init() {
	flag.Parse()
	if *help {
		flag.Usage()
		os.Exit(0)
	}

	err := core.ParseConf("config.json")
	if err != nil {
		if os.IsNotExist(err) {
			log.Println("please config config.json")
			os.Exit(0)
		}
		log.Panicln(err)
	}

	initDataBase()

	gin.SetMode(core.Conf.Mode)
}

func initDataBase() {
	db := database.Instance()

	createFood := !db.HasTable(&food.Food{})
	createUser := !db.HasTable(&userCenter.User{})
	createGroup := !db.HasTable(&userCenter.Group{})

	db.AutoMigrate(&userCenter.User{})
	db.AutoMigrate(&userCenter.Group{})
	db.AutoMigrate(&address.Address{})
	db.AutoMigrate(&category.Category{})
	db.AutoMigrate(&food.Food{})
	db.AutoMigrate(&order.Order{})

	if createGroup {
		userCenter.AddGroup("users", "user")
		userCenter.AddGroup("admin", "administration")
	}

	if createUser {
		userCenter.AddUser("admin", "admin", "admin", "", []string{"users", "admin"})
	}

	if createFood {
		initFoodDB(db)
	}
}

func initFoodDB(db *gorm.DB) {
	type FoodsData struct {
		Foods []struct{
			food.Food
			Category string `json:"category"`
		} `json:"foods"`
	}

	foodsData := FoodsData{}

	data, err := os.Open("data.json")
	if err != nil {
		return
	}
	err = json.NewDecoder(data).Decode(&foodsData)
	if err != nil {
		log.Println("加载 data.json 失败: "+err.Error())
		return
	}

	for _,f := range foodsData.Foods {
		id :=category.AddCategoryIfNotExist(f.Category)
		f.CategoryId = id
		food.AddFood(f.Food)
	}
}
