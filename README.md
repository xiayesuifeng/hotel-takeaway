# hotel-takeaway

> 网上酒楼外卖系统

[![pipeline status](https://gitlab.com/xiayesuifeng/hotel-takeaway/badges/master/pipeline.svg)](https://gitlab.com/xiayesuifeng/hotel-takeaway/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/xiayesuifeng/hotel-takeaway)](https://goreportcard.com/report/gitlab.com/xiayesuifeng/hotel-takeaway)
[![GoDoc](https://godoc.org/gitlab.com/xiayesuifeng/hotel-takeaway?status.svg)](https://godoc.org/gitlab.com/xiayesuifeng/hotel-takeaway)
[![Sourcegraph](https://sourcegraph.com/gitlab.com/xiayesuifeng/hotel-takeaway/-/badge.svg)](https://sourcegraph.com/gitlab.com/xiayesuifeng/hotel-takeaway)

## 前端

[hotel-takeaway-web](https://gitlab.com/xiayesuifeng/hotel-takeaway-web.git)

## 编译部署

### 编译
> 前端
```
git clone https://gitlab.com/xiayesuifeng/hotel-takeaway-web.git
cd hotel-takeaway-web
npm run install
npm run build
```
> 后端
```
go get gitlab.com/xiayesuifeng/hotel-takeaway
go build -ldflags "-s -w" gitlab.com/xiayesuifeng/hotel-takeaway
```

### 配置

> config.json详解

```json
{
  "mode":"debug",              //调试模式，正式部署改为release
  "database":{                 //数据库信息(暂只支持mysql,敬请期待别的数据库支持)
    "driver":"mysql",          //数据库驱动(支持mysql与portgres)
    "username":"root",         //数据库用户名
    "password":"",             //数据库密码
    "dbname":"hotel_takeaway", //数据库名(需要手动创建)
    "address":"127.0.0.1",     //数据库地址，当前为本地
    "port":"3306"              //数据库端口
  },
  "smtp":{                     //邮箱配置,用于支持发送验证码(尚未支持,无需配置,敬请期待)
    "username":"",
    "password": "",
    "host": ""
  }
}
```

> data.json详解(该文件为 `hotel-takeaway` 第一次启动初始化数据用)

```json
{
  "foods":[
    {
      "image": "https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwi_lbGazYfmAhVKu54KHfxnCeMQjRx6BAgBEAQ&url=https%3A%2F%2Fhealth.ettoday.net%2Fnews%2F1539897&psig=AOvVaw3AstlL2MSMEP6CEoh4Vj9r&ust=1574848291644903"  //食物图片url
      "name": "粉蒸肉",     //名字
      "category": "徽菜",	  //分类
      "description": "粉蒸肉，徽菜的代表，每个徽州人心底的一道能介绍给朋友们的一道美味佳肴，绝对能满足你的味蕾",  //描述
      "price": 200    //价格
    }
  ]
}
```

### 部署

#### 方案一
1. 将前端和后端、配置文件放到同一个路径下,如

```
./hotel-takeaway
├── config.json 					// 配置文件
├── data.json 					    // 第一次运行要导入的食物的配置文件
├── hotel-takeaway		 			// 后端
└── build							// 前端
```

2. 配置http服务器 (caddy为例)

```
example.com {
    root /your/path/hotel-takeaway/build
    gzip
    
    rewrite {
        if {path} not_match ^/api
        to {path} {path} /
    }
    proxy /api localhost:8080 {
        transparent
    }
}
```

3. 运行后端

```
hotel-takeaway -p 8080
```

#### 方案二
1. 将前端和后端、配置文件放到同一个路径下,如

```
./hotel-takeaway
├── config.json 					// 配置文件
├── data.json 					    // 第一次运行要导入的食物的配置文件
├── hotel-takeaway		 			// 后端
└── web							    // 前端
```
> 后端已内建静态服务器，所以前端也可放到 `web` 下

2. 运行后端
```
hotel-takeaway -p 8080
```

3. 配置http服务器 (caddy为例,可选)
```
example.com {
    proxy / localhost:8080 {
        transparent
    }
}
```

## License

hotel-takeaway is licensed under [GPLv3](LICENSE).