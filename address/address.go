package address

import (
	"errors"
	"github.com/jinzhu/gorm"
	"gitlab.com/xiayesuifeng/hotel-takeaway/database"
)

type Address struct {
	gorm.Model
	UserId uint
	Name  string `json:"name"`
	Age   string `json:"age"`
	Phone string `json:"phone"`
	Address string `json:"address"`
}

func AddAddress(address Address) error {
	db := database.Instance()
	return db.Create(&address).Error
}

func GetAddressByUserID(userID uint) []Address {
	db := database.Instance()
	address := make([]Address, 0)
	db.Where("user_id = ?",userID).Find(&address)
	return address
}

func GetAddressByID(userID uint,id int) (Address,error) {
	db := database.Instance()

	address := Address{}
	if db.Where("user_id = ? AND id = ?",userID,id).First(&address).RecordNotFound() {
		return address, errors.New("address not found")
	}

	return address, nil
}

func DeleteAddress(userID uint,id int) error {
	db := database.Instance()
	return db.Unscoped().Where("user_id = ? AND id = ?",userID,id).Delete(&Address{}).Error
}