package controller

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gitlab.com/xiayesuifeng/hotel-takeaway/core"
	"gitlab.com/xiayesuifeng/hotel-takeaway/order"
	"strconv"
)

type Order struct {

}

func (o *Order) Gets(ctx *gin.Context) {
	userID := o.checkUserID(ctx)
	if userID == 0 {
		return
	}

	ctx.JSON(200,core.SuccessDataResult("orders",order.GetOrdersByUserID(userID)))
}
func (o *Order) Get(ctx *gin.Context) {
	param := ctx.Param("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		ctx.JSON(200, core.FailResult("id must integer"))
		return
	}

	userID := o.checkUserID(ctx)
	if userID == 0 {
		return
	}

	if result,err := order.GetOrderByID(userID,id);err != nil {
		ctx.JSON(200,core.Result(core.ResultNotFoundCode,"order not found"))
	} else {
		ctx.JSON(200,core.SuccessDataResult("order",result))
	}
}

func (o *Order) Post(ctx *gin.Context) {
	userID := o.checkUserID(ctx)
	if userID == 0 {
		return
	}

	data := order.Order{}
	if err := ctx.ShouldBind(&data);err != nil {
		ctx.JSON(200,core.FailResult(err.Error()))
		return
	}

	data.UserId = userID
	data.Status = order.OrderWaitPaymentStatus

	if err := order.AddOrder(data);err != nil {
		ctx.JSON(200,core.FailResult(err.Error()))
	} else {
		ctx.JSON(200,core.SuccessResult())
	}
}

func (o *Order) Pay(ctx *gin.Context) {
	param := ctx.Param("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		ctx.JSON(200, core.FailResult("id must integer"))
		return
	}

	userID := o.checkUserID(ctx)
	if userID == 0 {
		return
	}

	if result,err := order.GetOrderByID(userID,id);err != nil {
		ctx.JSON(200,core.Result(core.ResultNotFoundCode,"order not found"))
	} else if err := result.Pay();err != nil {
		ctx.JSON(200,core.FailResult(err.Error()))
	} else {
		ctx.JSON(200,core.SuccessResult())
	}
}

func (o *Order) Complete(ctx *gin.Context) {
	param := ctx.Param("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		ctx.JSON(200, core.FailResult("id must integer"))
		return
	}

	userID := o.checkUserID(ctx)
	if userID == 0 {
		return
	}

	if result,err := order.GetOrderByID(userID,id);err != nil {
		ctx.JSON(200,core.Result(core.ResultNotFoundCode,"order not found"))
	} else if err := result.Complete();err != nil {
		ctx.JSON(200,core.FailResult(err.Error()))
	} else {
		ctx.JSON(200,core.SuccessResult())
	}
}

func (o *Order) Cancel(ctx *gin.Context) {
	param := ctx.Param("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		ctx.JSON(200, core.FailResult("id must integer"))
		return
	}

	userID := o.checkUserID(ctx)
	if userID == 0 {
		return
	}

	if result,err := order.GetOrderByID(userID,id);err != nil {
		ctx.JSON(200,core.Result(core.ResultNotFoundCode,"order not found"))
	} else {
		if result.Status >= 1 {
			ctx.JSON(200,core.FailResult("开始配送后不可取消"))
			return
		}
		if err := result.Cancel();err != nil {
			ctx.JSON(200,core.FailResult(err.Error()))
		} else {
			ctx.JSON(200,core.SuccessResult())
		}
	}
}

func (o *Order) Delete(ctx *gin.Context) {
	param := ctx.Param("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		ctx.JSON(200, core.FailResult("id must integer"))
		return
	}

	userID := o.checkUserID(ctx)
	if userID == 0 {
		return
	}

	if result,err := order.GetOrderByID(userID,id);err != nil {
		ctx.JSON(200,core.Result(core.ResultNotFoundCode,"order not found"))
	} else {
		if err := result.DeleteByUserID();err != nil {
			ctx.JSON(200,core.FailResult(err.Error()))
		} else {
			ctx.JSON(200,core.SuccessResult())
		}
	}
}

func (o *Order) checkUserID(ctx *gin.Context) uint {
	session := sessions.Default(ctx)
	tmp := session.Get("userID")
	if tmp == nil {
		ctx.JSON(200,core.Result(core.ResultUnauthorizedCode,"unauthorized"))
		return 0
	}

	return tmp.(uint)
}