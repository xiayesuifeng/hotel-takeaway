package controller

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gitlab.com/xiayesuifeng/hotel-takeaway/core"
	"gitlab.com/xiayesuifeng/hotel-takeaway/database"
	"gitlab.com/xiayesuifeng/hotel-takeaway/userCenter"
	"io"
	"os"
)

type AppApi struct {
}

type User struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

func (*AppApi) Login(ctx *gin.Context) {
	user := User{}

	if err := ctx.ShouldBind(&user); err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
		return
	}

	session := sessions.Default(ctx)

	u, err := userCenter.GetUserByName(user.Username)
	if err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
	}

	password := userCenter.EncryptionPassword(u.Username, user.Password, u.Email)

	if u.Password == password {
		session.Set("username", user.Username)
		session.Set("userID", u.ID)
		session.Save()

		u.Password = ""
		ctx.JSON(200, core.SuccessDataResult("user", u))
	} else {
		ctx.JSON(200, core.FailResult("username or password errors"))
	}
}

func (*AppApi) Signup(ctx *gin.Context) {
	type Data struct {
		User
		Nickname string `json:"nickname" binding:"required"`
		Email    string `json:"email" binding:"required"`
	}

	data := Data{}

	if err := ctx.ShouldBind(&data); err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
		return
	}

	groups := []string{"users"}

	db := database.Instance()

	if db.Where("email = ? AND state = pass", data.Email).RecordNotFound() {
		ctx.JSON(200, core.FailResult("this user is not eligible"))
	} else if err := userCenter.AddUser(data.Nickname, data.Username, data.Password, data.Email, groups); err != nil {
		ctx.JSON(200, core.FailResult("signup failure"))
	} else {
		ctx.JSON(200, core.SuccessResult())
	}
}

func (*AppApi) Logout(ctx *gin.Context) {
	session := sessions.Default(ctx)

	user := session.Get("username")
	if user != nil {
		session.Set("username", nil)
		session.Set("userID", nil)
		session.Save()
		ctx.JSON(200, core.SuccessResult())
	} else {
		ctx.JSON(200, core.Result(core.ResultUnauthorizedCode, "no login"))
	}

}

func (*AppApi) UploadAvatar(ctx *gin.Context) {
	session := sessions.Default(ctx)

	username := ctx.PostForm("username")
	user := session.Get("username")
	if user != nil && username == user.(string) {
		avatar, _, err := ctx.Request.FormFile("avatar")
		if err != nil {
			ctx.JSON(200, core.FailResult(err.Error()))
		} else {
			file, err := os.Create("./assets/avatar/" + username)
			if err != nil {
				ctx.JSON(200, core.FailResult(err.Error()))
				return
			}

			defer file.Close()

			io.Copy(file, avatar)
			ctx.JSON(200, core.SuccessResult())
		}
	} else {
		ctx.JSON(200, core.Result(core.ResultUnauthorizedCode, "permission denied"))
	}

}

func (*AppApi) GetAvatar(ctx *gin.Context) {
	username := ctx.Param("username")
	path := "./assets/avatar/" + username
	if _, err := os.Stat(path); err != nil {
		ctx.File("./assets/avatar/default.svg")
	} else {
		ctx.File(path)
	}
}
