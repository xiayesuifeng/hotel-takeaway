package controller

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gitlab.com/xiayesuifeng/hotel-takeaway/address"
	"gitlab.com/xiayesuifeng/hotel-takeaway/core"
	"strconv"
)

type Address struct {
}

func (a *Address) Post(ctx *gin.Context) {
	session := sessions.Default(ctx)
	tmp := session.Get("userID")
	if tmp == nil {
		ctx.JSON(200,core.Result(core.ResultUnauthorizedCode,"unauthorized"))
		return
	}

	data := address.Address{}
	if err := ctx.ShouldBind(&data); err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
		return
	}

	data.UserId = tmp.(uint)

	if err := address.AddAddress(data); err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
	} else {
		ctx.JSON(200, core.SuccessResult())
	}
}

func (a *Address) Gets(ctx *gin.Context) {
	session := sessions.Default(ctx)
	tmp := session.Get("userID")
	if tmp == nil {
		ctx.JSON(200,core.Result(core.ResultUnauthorizedCode,"unauthorized"))
		return
	}

	ctx.JSON(200, core.SuccessDataResult("address", address.GetAddressByUserID(tmp.(uint))))
}

func (a *Address) Get(ctx *gin.Context) {
	param := ctx.Param("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		ctx.JSON(200, core.FailResult("id must integer"))
		return
	}

	session := sessions.Default(ctx)
	tmp := session.Get("userID")
	if tmp == nil {
		ctx.JSON(200,core.Result(core.ResultUnauthorizedCode,"unauthorized"))
		return
	}

	data, err := address.GetAddressByID(tmp.(uint),id)
	if err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
	} else {
		ctx.JSON(200, core.SuccessDataResult("address", data))
	}
}

func (a *Address) Delete(ctx *gin.Context) {
	param := ctx.Param("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		ctx.JSON(200, core.FailResult("id must integer"))
		return
	}

	session := sessions.Default(ctx)
	tmp := session.Get("userID")
	if tmp == nil {
		ctx.JSON(200,core.Result(core.ResultUnauthorizedCode,"unauthorized"))
		return
	}

	if err := address.DeleteAddress(tmp.(uint),id); err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
	} else {
		ctx.JSON(200, core.SuccessResult())
	}
}
