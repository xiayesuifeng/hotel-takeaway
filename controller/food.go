package controller

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/xiayesuifeng/hotel-takeaway/core"
	"gitlab.com/xiayesuifeng/hotel-takeaway/database"
	"gitlab.com/xiayesuifeng/hotel-takeaway/food"
	"strconv"
)

type Food struct {
}

func (f *Food) Post(ctx *gin.Context) {
	data := food.Food{}
	if err := ctx.ShouldBind(&data); err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
		return
	}

	if err := food.AddFood(data); err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
	} else {
		ctx.JSON(200, core.SuccessResult())
	}
}

func (f *Food) Gets(ctx *gin.Context) {
	ctx.JSON(200, core.SuccessDataResult("foods", food.GetFoods()))
}

func (f *Food) Get(ctx *gin.Context) {
	param := ctx.Param("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		ctx.JSON(200, core.FailResult("id must integer"))
		return
	}

	data, err := food.GetFood(id)
	if err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
	} else {
		ctx.JSON(200, core.SuccessDataResult("food", data))
	}
}

func (f *Food) Put(ctx *gin.Context) {
	param := ctx.Param("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		ctx.JSON(200, core.FailResult("id must integer"))
		return
	}

	data, err := food.GetFood(id)
	if err != nil {
		ctx.JSON(200, core.FailResult("food not found"))
		return
	}

	if err := ctx.ShouldBind(&data); err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
		return
	}

	if err := data.EditFood(); err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
	} else {
		ctx.JSON(200, core.SuccessResult())
	}
}

func (f *Food) Delete(ctx *gin.Context) {
	param := ctx.Param("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		ctx.JSON(200, core.FailResult("id must integer"))
		return
	}

	if err := food.DeleteFood(id); err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
	} else {
		ctx.JSON(200, core.SuccessResult())
	}
}

func (f *Food) GetByCategory(ctx *gin.Context) {
	param := ctx.Param("category_id")
	id, err := strconv.Atoi(param)
	if err != nil {
		ctx.JSON(200, core.FailResult("id must integer"))
		return
	}

	db := database.Instance()
	foods := make([]food.Food, 0)

	db.Where("category_id = ?", id).Order("created_at DESC").Find(&foods)

	ctx.JSON(200, core.SuccessDataResult("foods", foods))
}