package controller

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gitlab.com/xiayesuifeng/hotel-takeaway/core"
	"gitlab.com/xiayesuifeng/hotel-takeaway/userCenter"
	"strconv"
)

type UserCenter struct {
}

func (u *UserCenter) Post(ctx *gin.Context) {
	type Data struct {
		Nickname string `json:"nickname" binding:"required"`
		Username     string   `json:"username" binding:"required"`
		Password     string   `json:"password" binding:"required"`
		Email        string   `json:"email" binding:"required"`
		Group        []string `json:"group" binding:"required"`
	}

	data := Data{}

	if err := ctx.Bind(&data); err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
		return
	}

	if err := userCenter.AddUser(data.Nickname,data.Username, data.Password, data.Email,data.Group); err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
	} else {
		ctx.JSON(200, core.SuccessResult())
	}
}

func (u *UserCenter) Delete(ctx *gin.Context) {
	param := ctx.Param("id")

	id, err := strconv.Atoi(param)
	if err != nil {
		ctx.JSON(200, core.FailResult("id must integer"))
		return
	}

	user, err := userCenter.GetUserById(id)
	if err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
		return
	}

	if err = user.Delete(); err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
	} else {
		ctx.JSON(200, core.SuccessResult())
	}
}

func (u *UserCenter) Get(ctx *gin.Context) {
	users, err := userCenter.GetUser()
	if err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
		return
	}

	ctx.JSON(200, core.SuccessDataResult("users", users))
}

func (u *UserCenter) Put(ctx *gin.Context) {
	id := ctx.Param("id")

	i, err := strconv.Atoi(id)
	if err != nil {
		ctx.JSON(200, core.FailResult("id must integer"))
		return
	}

	user, err := userCenter.GetUserById(i)

	session := sessions.Default(ctx)

	tmp := session.Get("username")
	if tmp == nil {
		ctx.JSON(200, core.Result(core.ResultUnauthorizedCode, "unauthorized"))
		return
	}

	if user.Username != tmp.(string) {
		ctx.JSON(200, core.Result(core.ResultUnauthorizedCode, "unauthorized"))
		return
	}

	type Data struct {
		Password string `json:"password" form:"password"`
	}

	data := Data{}

	if err := ctx.ShouldBind(&data); err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
		return
	}

	if err = user.Edit(user.Nickname,data.Password); err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
	} else {
		ctx.JSON(200, core.SuccessResult())
	}
}

func (u *UserCenter) AddGroup(ctx *gin.Context) {
	type Data struct {
		Name        string `json:"name" form:"name" binding:"required"`
		Description string `json:"description" form:"description" binding:"required"`
	}

	data := Data{}

	if err := ctx.ShouldBind(&data); err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
		return
	}

	if err := userCenter.AddGroup(data.Name, data.Description); err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
	} else {
		ctx.JSON(200, core.SuccessResult())
	}
}

func (u *UserCenter) DeleteGroup(ctx *gin.Context) {
	name := ctx.Param("name")

	if err := userCenter.DeleteGroup(name); err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
	} else {
		ctx.JSON(200, core.SuccessResult())
	}
}

func (u *UserCenter) GetGroup(ctx *gin.Context) {
	groups, err := userCenter.GetGroup()
	if err != nil {
		ctx.JSON(200, core.FailResult(err.Error()))
		return
	}

	ctx.JSON(200, core.SuccessDataResult("groups", groups))
}
