package order

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/xiayesuifeng/hotel-takeaway/database"
	"gitlab.com/xiayesuifeng/hotel-takeaway/food"
)

type Order struct {
	gorm.Model
	UserId    uint         `json:"userId"`
	Foods     []food.Food `gorm:"many2many:order_food" json:"foods"`
	AddressId int         `json:"addressId"`
	Status    int         `json:"status"`
}

const (
	OrderWaitPaymentStatus = iota
	OrderWaitDeliveryStatus
	OrderFinishedStatus
	OrderCancelStatus
)

func AddOrder(order Order) error {
	db := database.Instance()
	return db.Create(&order).Error
}

func GetOrdersByUserID(userID uint) (orders []Order) {
	db := database.Instance()
	db.Where("user_id=?", userID).Preload("Foods").Find(&orders)

	return orders
}

func GetOrderByID(userID uint, ID int) (order Order, err error) {
	db := database.Instance()
	if db.Where("user_id=? AND id=?", userID, ID).Preload("Foods").First(&order).RecordNotFound() {
		return Order{}, db.Error
	}

	return order, nil
}

func (o Order) Pay() error {
	db := database.Instance()
	return db.Model(&o).Update(Order{Status: OrderWaitDeliveryStatus}).Error
}

func (o Order) Complete() error {
	db := database.Instance()
	return db.Model(&o).Update(Order{Status: OrderFinishedStatus}).Error
}

func (o Order) Cancel() error {
	db := database.Instance()
	return db.Model(&o).Update(Order{Status: OrderCancelStatus}).Error
}

func (o Order) DeleteByUserID() error {
	db := database.Instance()
	return db.Unscoped().Delete(&o,"user_id=? AND id=?", o.UserId, o.ID).Error
}