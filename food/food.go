package food

import (
	"errors"
	"github.com/jinzhu/gorm"
	"gitlab.com/xiayesuifeng/hotel-takeaway/database"
)

type Food struct {
	gorm.Model
	Name string `gorm:"type:varchar(100);unique" json:"name"`
	Description string `json:"description"`
	Price float32 `json:"price"`
	CategoryId uint `json:"categoryId"`
	Image string `json:"image"`
}

func AddFood(food Food) error {
	db := database.Instance()
	return db.Create(&food).Error
}

func GetFoods() []Food {
	db := database.Instance()
	foods := make([]Food, 0)
	db.Find(&foods)
	return foods
}

func GetFood(id int) (Food, error) {
	db := database.Instance()
	food := Food{}
	if db.Find(&Food{}, id).RecordNotFound() {
		return food, errors.New("food not found")
	}

	return food, nil
}

func (f *Food) EditFood() error {
	db := database.Instance()

	return db.Model(&f).Update(Food{Name:f.Name,CategoryId:f.CategoryId}).Error
}

func DeleteFood(id int) error {
	db := database.Instance()
	return db.Unscoped().Delete(&Food{}, id).Error
}